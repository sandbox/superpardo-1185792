<?php
/**
 * @file
 * Page callbacks for adding, editing, deleting, and revisions appointments
 */

/**
 * Menu callback; prints a cretion appointment form
 */
function appointment_page() {
  $output=drupal_get_form('appointment_form');
  return $output;
}

/**
 * Form builder for the appointment creation
 *
 * @ingroup forms
 */
function appointment_form() {
  $form=array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('General Information'),
    '#collapsible' => FALSE,
    '#collapsed' => FALSE,
  );
  $form['general']['name'] = array(
    '#type' => 'textfield',
    '#title' => t('Name'),
    '#size' => 60,
    '#required' => TRUE,
  );
  $form['general']['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last name'),
    '#size' => 60,
    '#required' => TRUE,
  );
  $form['general']['birthday'] = array(
    '#type' => 'date_popup',
    '#title' => t('Birthday'),
    '#date_format' => 'Y-d-m',
    '#date_year_range' => '-100:+0',
  );
  $form['general']['phone'] = array(
    '#type' => 'textfield',
    '#title' => t('Phone'),
    '#size' => 60,
    '#required' => TRUE,
  );
  $form['general']['email'] = array(
    '#type' => 'textfield',
    '#title' => t('Email'),
    '#size' => 60,
    '#required' => TRUE,
  );
  $form['general']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Get date'),
  );  
  return $form;
}

/**
 * Validate appointment form submissions.
 *
 * Check if the phone is numeric.
 * Chech the email existence
 * Check the email is valid
 */
function appointment_form_validate( $form, &$form_state) {
  $unique_email = variable_get('appointment_unique_email', '');
  $mail     = trim($form_state['values']['email']);
  $telefono = $form_state['values']['phone'];
  if ( !_validate_email( $mail ) ) {
    form_set_error('general][email', t('The email address appears to be invalid.'));
  }
  if ( $unique_email ) {
    if ( !_verify_existence( $mail ) ) {
      form_set_error('general][email', t('The email address is already used'));
    }
  }
  if (!is_numeric($telefono)) {
    form_set_error('general][phone', t('The telephone number appears to be invalid.'));
  }
}

/**
 * Form submission handler for appointment_form().
 */
function appointment_form_submit( $form, &$form_state) {
  $cita = _create_appointment();
  $ultima_cita  = _get_last_appointment();
  $total_appointments = variable_get('appointment_total', '');
  $hora_vieja = $ultima_cita->hora_cita;
  $aux_hora = split(' ', $cita);
  $hora = $aux_hora[1];
  $record = new stdClass();
  $record->name = $form_state['values']['name'];
  $record->last_name = $form_state['values']['last_name'];
  $record->birthday = $form_state['values']['birthday'];
  $record->phone = $form_state['values']['phone'];
  $record->email = $form_state['values']['email'];
  drupal_write_record('appointment', $record);
  $ccid = db_last_insert_id('appointment', 'ccid');
  $record=new stdClass();
  $record->appointment_date = $cita;
  $record->appointment_time = $hora;
  drupal_write_record('appointment_date', $record);
  $cfid = db_last_insert_id('appointment_date', 'cfid');
  $record=new stdClass();
  $record->ccid = $ccid;
  $record->cfid = $cfid;
  drupal_write_record('appointment', $record, array('ccid'));
  $ultimo_folio = _get_last_folio();
  $repeat = variable_get('appointment_repeat_folios', '');
  if ( $repeat ) {
    if ( !empty( $ultimo_folio ) ) {
      $_aux_hora = split(' ', $cita);
      if ( $hora_vieja >= $_aux_hora[1] || $ultimo->folio == $total_appointments ) {
        $next_folio = 1;
      }
      else {
        $next_folio = $ultimo_folio->folio + 1;
      }
    }
    else {
      $next_folio = 1;
    }
  }
  else {
    $next_folio = $ultimo_folio->folio + 1;
  }
  $record=new stdClass();
  $record->cfid  = $cfid;
  $record->folio = $next_folio;
  drupal_write_record('appointment_folio', $record);
  drupal_goto('appointment/confirmation/'. $ccid);
}
