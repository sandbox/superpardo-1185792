<?php
/**
 * @file
 * Content administration and module settings UI.
 */

/**
 * Form builder for the appointment settings page.
 *
 * @see system_settings_form()
 * @ingroup forms
 */
function appointment_admin_settings() {
  $form = array();
  $form['general'] = array(
    '#type' => 'fieldset',
    '#weight' => -20,
    '#title' => t('Configurations'),
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );
  $form['general']['appointment_total'] = array(
    '#type' => 'textfield',
    '#title' => t('Total of appointments'),
    '#size' => 60,
    '#description' => t('Appointments per day'),
    '#required' => TRUE,
    '#default_value' => variable_get('appointment_total', ''),
  );
  $form['general']['appointment_start_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Start time'),
    '#size' => 60,
    '#description' => t('The time at which they start to reserve appointments. The format should be 8:30'),
    '#required' => TRUE,
    '#default_value' => variable_get('appointment_start_time', ''),
  );
  $form['general']['appointment_close_time'] = array(
    '#type' => 'textfield',
    '#title' => t('Close time'),
    '#size' => 60,
    '#description' => t('The time at ending the reservation of appointments. The format should be 16:30'),
    '#required' => TRUE,
    '#default_value' => variable_get('appointment_close_time', ''),
  );
  $form['general']['appointment_unique_email'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Multiple appointments per user.'),
    '#default_value' => variable_get('appointment_unique_email', ''),
    '#description' => t('Allows that the user create multiple appointemnts.'),
  );
  $form['general']['appointment_repeat_folios'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Repeat folios per day.'),
    '#default_value' => variable_get('appointment_repeat_folios', ''),
    '#description' => t('Allows repeat the folios on different days'),
  );
  $form['general']['appointment_saturday'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Create appointment on Saturdays.'),
    '#default_value' => variable_get('appointment_saturday', ''),
    '#description' => t('Allows to create appointemnts on Saturday'),
  );
  $form['general']['appointment_sunday'] = array(
    '#type' => 'checkbox', 
    '#title' => t('Create appointment on Sundays.'),
    '#default_value' => variable_get('appointment_sunday', ''),
    '#description' => t('Allows to create appointemnts on Sunday'),
  );
/*
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );  
*/
  return system_settings_form($form);
}

/**
 * Form builder for the appointment holidays settings page.
 *
 * @ingroup forms
 */
function appointment_holidays_admin_settings() {
  $form = array();
  $fechas = _get_holidays();
  for ( $x = 0 ; $x < 15 ; $x++ ) {
    $form['general']['holiday'][$x] = array(
      '#type' => 'date_popup',
      '#title' => t('Holiday'),
      '#date_format' => 'Y-d-m',
      '#date_year_range' => '-100:+0',
      '#default_value' => variable_get("appointment_holiday_$x", ''),
    );
  }
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Guardar'),
  );  
  return $form;
}

/**
 * Form submission handler for appointment_holidays_admin_settings().
 */
function appointment_holidays_admin_settings_submit($form, &$form_state) {
  for ( $x = 0 ; $x < 15 ; $x++ ) {
    variable_set("appointment_holiday_$x", $form_state['values'][$x]);
  }
}

/**
 * Form builder: Builds the appointment administration overview.
 */
function appointment_admin_content() {
  $result = pager_query(db_rewrite_sql('SELECT a.*, ad.*, af.* FROM {appointment} a INNER JOIN {appointment_date} ad ON ad.cfid = a.cfid INNER JOIN {appointment_folio} af ON af.cfid = ad.cfid ORDER BY ad.appointment_date DESC'), 50, 0, NULL);
  $options = array();
  foreach ( appointment_operations() as $operation => $array) {
    $options[$operation] = $array['label'];
  }
  $form['options'] = array(
    '#type' => 'fieldset',
    '#title' => t('Update options'),
    '#prefix' => '<div class="container-inline">',
    '#suffix' => '</div>',
  );
  $form['options']['operation'] = array(
    '#type' => 'select',
    '#options' => $options,
    '#default_value' => 'Active',
  );
  $form['options']['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Update'),
    '#submit' => array('appointment_admin_content_submit'),
  );

  while ($appointment = db_fetch_object($result)) {
    $appointments[$appointment->ccid] = '';
    $form['name'][$appointment->ccid] =  array('#value' => check_plain($appointment->name));
    $form['last_name'][$appointment->ccid] =  array('#value' => check_plain($appointment->last_name));
    $form['appointment_date'][$appointment->ccid] =  array('#value' => check_plain($appointment->appointment_date));
    $form['status'][$appointment->ccid] =  array('#value' => ($appointment->status ? t('Active') : t('Inactive')));
  }
  $form['appointments'] = array('#type' => 'checkboxes', '#options' => $appointments);
  $form['pager'] = array('#value' => theme('pager', NULL, 50, 0));
  $form['#theme'] = 'appointment_admin_content';
  return $form;
}

/**
 * Form submission handler for appointment_admin_content().
 */
function appointment_admin_content_submit( $form, &$form_state) {
  $operation = $form_state['values']['operation'];
  $appointments = $form_state['values']['appointments'];
  if ( $operation = 'active' ) {
    $update = 1;
  }
  else {
    $update = 0;
  }
  foreach ( $appointments as $appointment ) {
    if ( $appointment != 0) {
      $record = new stdClass();
      $record->ccid = $appointment;
      $record->status = $update;
      drupal_write_record('appointment', $record, array('ccid'));
    }
  }
}

/**
 * Theme appointment administration overview.
 *
 * @ingroup themeable
 */
function theme_appointment_admin_content($form) {
  $has_posts = isset($form['name']) && is_array($form['name']);
  $select_header = $has_posts ? theme('table_select_header_cell') : '';
  $header = array($select_header, t('Name'), t('Last name'), t('Appointment'), t('Status'));
  $output = '';
  $output .= drupal_render($form['options']);
  if ($has_posts) {
    foreach (element_children($form['name']) as $key) {
      $row = array();
      $row[] = drupal_render($form['appointments'][$key]);
      $row[] = drupal_render($form['name'][$key]);
      $row[] = drupal_render($form['last_name'][$key]);
      $row[] = drupal_render($form['appointment_date'][$key]);
      $row[] = drupal_render($form['status'][$key]);
      $rows[] = $row;
    }
  }
  else {
    $rows[] = array(array('data' => t('No appointments available.'), 'colspan' => '6'));
  }
  $output .= theme('table', $header, $rows);
  if ($form['pager']['#value']) {
    $output .= drupal_render($form['pager']);
  }
  $output .= drupal_render($form);
  return $output;
}

/**
 * Get all operations for the page overview
 *
 * @return array
 *   The list of all operations
 */
function appointment_operations() {
  $operations = array(
    'active' => array(
      'label' => t('Active'),
    ),
    'inactive' => array(
      'label' => t('Inactive'),
    ),
  );
  return $operations;
}
