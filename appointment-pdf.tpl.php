<?php
/**
 * @file appointment-pdf.tpl.php
 * Default theme implementation to present appointment confirmation.
 *
 * This template is used when viewing a appointment confirmation,
 * e.g., example.com/appointment/confirmation/123/pdf. 123 being appointment ID.
 *
 * By default, all appointment data is printed out with the $appointment
 * variable. 
 *
 * Available variables:
 *   - $appointment: Keyed array of appointment data.
 *
 * To check for all available data within $appointment, use the code below.
 * @code
 *   print '<pre>'. check_plain(print_r($appointment, 1)) .'</pre>';
 * @endcode
 *
 * @see template_preprocess_appointment()
 */
?>
<div>
<div><b>Folio: </b><p><?php print $appointment->ccid ?></p></div>
<div><b>Name: </b><p><?php print $appointment->name ?></p></div>
<div><b>Date: </b><p><?php print $appointment->appointment_date ?></p></div>
</div>
