-- SUMMARY --

The module can create appointments and keep track of them.
Include its features are:

* Set up an e-mail itself can create more than one appointment
* Set weekends
* Configure holidays
* Set up appointments
* Set the repetition of pages per day
* Set the start time and end time for appointments

Has an administration panel to change your dating status.
Select the date via a calendar with jQuery UI.

Drupal 7
No release date.

-- REQUIREMENTS --

* Date module.
* Jquery UI Module
* TCPDF library

-- INSTALLATION --

* Copy the appointment module directory to your sites/all/modules directory, so it
  is located in sites/all/modules/appointment/.

* Download the TCPDF library from:
  http://sourceforge.net/projects/tcpdf/files/

* Put the downloaded archive into the module directory:
  /sites/all/modules/appointment/tcpdf_5_9_088.zip

* Extract the archive.  This will create the following sub-directory:
    /sites/all/modules/appointment/tcpdf_5_9_088/

* Rename the sub-directory into "tcpdf" within the appointment module folder:
    /sites/all/modules/appointment/tcpdf/

* Enable the module at Administer >> Site building >> Modules.
